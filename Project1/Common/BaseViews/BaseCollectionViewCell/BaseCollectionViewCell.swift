//
//  BaseCollectionViewCell.swift
//  Project1
//
//  Created by NguyenHao on 12/10/2023.
//

import UIKit

public class BaseCollectionViewCell: UICollectionViewCell, Reusable {
    public override func awakeFromNib() {
        setupView()
    }
    
    public func setupView() {
        
    }
}
