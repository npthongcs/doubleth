//
//  BaseViewController.swift
//  Project1
//
//  Created by NguyenHao on 12/10/2023.
//

import UIKit

open class BaseViewController: UIViewController {
    open override func viewDidLoad() {
        setupView()
    }
    
    open func setupView() {}
}
