//
//  typealias.swift
//  Project1
//
//  Created by NguyenHao on 12/10/2023.
//

import UIKit

typealias ImageCallBack = (UIImage?) -> Void
